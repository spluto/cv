all:
	xelatex  -jobname=out/cv_en cv_en.tex
	xelatex  -jobname=out/cv_ua cv_ua.tex
	xelatex  -jobname=out/cv_ru cv_ru.tex
	find out -type f ! -name '*.pdf' -delete
